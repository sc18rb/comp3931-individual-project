# COMP3931 - Individual Project



## Overview
The pipeline of the solution is built using a web-based interactive computing platform Jupyter
Notebook. This open-source platform allows to conveniently import datasets, pre-process
data, train and evaluate the model in a single notebook without the need to re-compile all
available code. The machine that runs the notebooks is a Asus ZenBook that has a Intel(R)
Core(TM) i5-8250U CPU @ 1.60GHz, 1800 Mhz, 4 Core(s), 8 Logical Processors and 8 GB of
Random-Access Memory (RAM). The models are trained with the NVIDIA GeForce MX150
graphics card.

## Notebooks and RNNs

Two different types of Recurrent Neural Networks were used in this project - Long Short-Term
Memory (LSTM) and Sequence to Sequence Network with Attention. There are two notebooks dedicated each for both LSTM and Seq2Seq. One is to train the model with integrated technical indicators, the other one without.

Notebooks that train the models without technical indicators:

LSTM-Multivariate-GPU.ipynb

Seq2Seq-Multivariate-GPU.ipynb

Notebooks that train the models with technical indicators:

LSTM-Multivariate-withTA-GPU.ipynb

Seq2Seq-Multivariate-withTA-GPU.ipynb

## Datasets
APL, IBEX and SPY datasets used in this project were pulled from Yahoo Finance, using yahoofinancials python module.

## Feature spaces
Basic model feature space: Open, Close, Adj Close, Open, Close, Adj Close, High, Low, Volume (6 in total)


Model with TIs feature space: Open, Close, Adj Close, High, Low, Volume EMA8, EMA14, EMA50, EMA200, StochRSIK, StochRSID, MACD, MACDH, MACDS, ATR (16 in total)

## Time-windows
Short-term: historical data from 2017 to 2022

Medium-term: historical data from 2007 to 2022

Long-term: historical data from 1997 to 2022

## Models
Two different types of RNNs, three different datasets, two feature spaces and three different time-windows were used. Therefore, in total 2*3*2*3 = 36 models were trained that can be found in the Models section of this repository.
